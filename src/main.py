import pygame as pg
import numpy as np
import cv2
from pygame import gfxdraw as gfx


pg.init()


SIZE = (640, 480)

BLACK = (0, 0, 0)
WHITE = (0xFF, 0xFF, 0xFF)

RED = (0xFF, 0, 0)
GREEN = (0, 0xFF, 0)
BLUE = (0, 0, 0xFF)

CYAN = (0, 0xFF, 0xFF)
MAGENTA = (0xFF, 0, 0xFF)
YELLOW = (0xFF, 0xFF, 0)


def circle(surface, center, radius, color):
    cx, cy = center
    gfx.filled_circle(surface, cx, cy, radius, color)
    gfx.aacircle(surface, cx, cy, radius, color)


def line(surface, start, end, color):
    pg.draw.aaline(surface, color, start, end)


def polygon(surface, points, color):
    gfx.filled_polygon(surface, points, color)
    gfx.aapolygon(surface, points, color)


def straight(surface, base, dir, color):
    top = left = 0
    right, bottom = SIZE

    dx, dy = dir
    bx, by = base
    if abs(dx) < abs(dy):
        sn, en = ((y - by) / dy for y in [top, bottom])
    else:
        sn, en = ((x - bx) / dx for x in [left, right])
    start = tuple(round(b + sn * d) for b, d in zip(tuple(base), tuple(dir)))
    end = tuple(round(b + en * d) for b, d in zip(tuple(base), tuple(dir)))
    line(surface, start, end, color)


def simplify(points):
    for curr, next in zip(points, points[1:] + [points[-1]]):
        pass


def mass_point(points):
    points = np.asarray(points, dtype=np.int32)

    min = np.min(points, axis=0)
    max = np.max(points, axis=0)
    points -= min

    arr = np.zeros(max - min, dtype=np.uint8)
    cv2.fillPoly(arr, pts=points[np.newaxis, :, ::-1], color=0x1)
    x = np.nonzero(arr)
    return (np.average(x, axis=1) + min).astype(np.int32)


def halving_line(points, angle=0):
    s, c = np.sin(angle), np.cos(angle)
    rot = np.array(
        [
            [c, -s],
            [s, c],
        ]
    )
    rotinv = np.array(
        [
            [c, s],
            [-s, c],
        ]
    )

    points = np.array(points, dtype=np.int32)
    points = np.matmul(rot[np.newaxis, ...], points[:, :, np.newaxis]).reshape(points.shape)
    xy = _halving_line(points)
    return rotinv @ xy


def _halving_line(points):
    points = np.asarray(points, dtype=np.int32)

    min = np.min(points, axis=0)
    max = np.max(points, axis=0)
    points -= min

    arr = np.zeros(max - min, dtype=np.uint8)
    cv2.fillPoly(arr, pts=points[np.newaxis, :, ::-1], color=0xFF)
    col_sums = np.sum(arr, axis=0)
    accum_col_sums = np.cumsum(col_sums)

    y = np.searchsorted(accum_col_sums, accum_col_sums[-1] / 2)
    arr[:, y] = 0x7F
    # cv2.imshow("test", np.rot90(arr)[::-1])
    # cv2.waitKey(1)
    return (0, y) + min


def main():
    pg.event.set_blocked(None)
    pg.event.set_allowed([pg.QUIT, pg.MOUSEBUTTONUP, pg.MOUSEMOTION, pg.KEYDOWN, pg.KEYUP])

    display = pg.display.set_mode(size=SIZE)
    pg.display.set_caption("PolygonCenter")
    points = []
    straights = []
    s_down = False

    while True:
        display.fill(BLACK)

        # draw polygon
        match points:
            case [] | [_]:
                pass
            case [pointa, pointb]:
                line(display, pointa, pointb, RED)
            case points:
                polygon(display, points, RED)
        for p in points:
            circle(display, p, 2, CYAN)

        # draw mouse preview
        if len(points) >= 2:
            polygon(display, [points[0], points[-1], pg.mouse.get_pos()], (*RED, 0x4F))

        # draw centers
        if len(points) >= 2:
            circle(display, mass_point(points), 2, YELLOW)
        for base, dir in straights:
            straight(display, base, dir, GREEN)

        pg.display.flip()

        for event in pg.event.get():
            if event.type == pg.QUIT:
                return
            # left mouse click or left mouse button held
            elif (event.type == pg.MOUSEBUTTONUP and event.button == 1) or (
                event.type == pg.MOUSEMOTION and event.buttons[0]
            ):
                points.append(event.pos)
                straights = []
            elif event.type == pg.KEYDOWN and event.key == pg.K_SPACE:
                points = []
                straights = []
            elif event.type == pg.KEYDOWN and event.key == pg.K_s:
                s_down = True
            elif event.type == pg.KEYUP and event.key == pg.K_s:
                s_down = False

        if s_down:
            angle = np.random.uniform(0, np.pi)
            dir = np.cos(angle), -np.sin(angle)
            base = halving_line(points, angle)
            straights.append((base, dir))


if __name__ == "__main__":
    main()
